**Anggota kelompok B-04**

1. Fadhli Gazalba - 2006525904
2. Kesya Aurelya - 2006486046
3. Mochamad Thariq Zahir Abdillah - 2006486185
4. Mohammad Bramantyo Putra Kusuma - 2006520903
5. Pramudya Wibisono - 2006526106
6. Salma Karimah - 2006529934
7. Syahdan Putra Adriatama - 2006486001

**Link :** [APK](https://drive.google.com/drive/u/1/folders/1reYrMKDygf-t1UF6rO_gocnHTU3LvEA6)


**Cerita Aplikasi yang Diajukan serta Manfaatnya**


Tak terasa, pandemi Covid-19 sudah hampir dua tahun “menemani” masyarakat Indonesia. Pandemi Covid-19 memberikan dampak yang sangat besar pada seluruh sektor esensial di dunia, khususnya Indonesia. Lambat laun, pemerintah Indonesia meminta agar masyarakat harus siap untuk hidup berdampingan dengan Covid-19. Peluncuran aplikasi PeduliLindungi merupakan salah satu aksi nyata pemerintah untuk menekan laju Covid-19 dan sebagai persiapan bagi masyarakat untuk hidup berdampingan dengan Covid-19. Oleh karena itu, kelompok kami membuat sebuah aplikasi yang dinamakan Zona Hijau. Sesuai namanya, website ini memiliki esensi agar Indonesia dapat terbebas dari wabah Covid-19 (zona hijau) di seluruh wilayah Indonesia. Zona Hijau diharapkan mampu membantu masyarakat dalam hidup berdampingan dengan Covid-19 sekaligus untuk mengurangi wabah tersebut.
Zona Hijau merupakan suatu aplikasi yang bertujuan untuk memberikan informasi dan edukasi mengenai Covid-19. Untuk mencapai tujuan tersebut, aplikasi ini akan menampilkan data-data penting terkait persebaran Covid-19, seperti data kasus hari ini, jumlah kenaikan kasus, daftar rumah sakit atau puskesmas rujukan, jawaban dari pertanyaan-pertanyaan seputar Covid-19 yang sering ditanyakan, dan lainnya. Selain memberikan informasi data, aplikasi ini juga memberikan konten-konten edukasi berbentuk infografis dan video mengenai tips kesehatan dan konten lain yang bertujuan untuk meningkatkan kesadaran dan pengetahuan masyarakat terhadap persebaran Covid-19. Selain itu, website ini juga menyediakan fitur-fitur yang melibatkan pengguna secara langsung melalui fitur pesan dimana pengguna dapat berbagi pengalaman, pengetahuan, ataupun perhatian kepada pengguna lainnya.

**Modul yang akan Diimplementasikan dan bagaimana integrasinya dengan Web Service atas halaman Web sebelumnya.**

1. Dashboard

Pada halaman ini, pengguna akan mendapatkan informasi mengenai data kasus Covid-19 di Indonesia secara real time. Untuk itu diperlukan pengambilan data dari dua API yang sama seperti yang saat melakukan pengerjaan web, yaitu: 
https://apicovid19indonesia-v2.vercel.app/api/indonesia/provinsi/ untuk data provinsi
https://api.kawalcorona.com/indonesia/ untuk data seluruh Indonesia
Kemudian terdapat juga form feedback yang meminta pesan berupa kritik atau saran yang dapat ditampilkan kepada pengguna seperti implementasi di web sebelumnya. 

2. Tips Menghindari Covid (infografis & video)

Pada fitur ini, pengguna dapat melihat konten edukasi mengenai Covid-19 berupa infografis mengenai tips menghindari Covid-19. Selain itu, pengguna juga dapat memilih video yang akan ditonton dengan menggunakan button next atau previous, video dibagi dengan beberapa rangkaian olahraga, seperti push up, sit up, plank, dll. 
Data video diambil dari database yang sama dengan halaman web yaitu pada https://zonahijau.herokuapp.com/videoinfog/json/ sehingga interpretasinya pada flutter nanti akan sama dengan yang ada pada web,

3. Frequently Asked Questions (FAQ)

Pada fitur ini, permasalahan mengenai Covid-19 yang sering ditanyakan akan ditampilkan beserta dengan jawabannya sehingga pengguna tidak perlu repot untuk menanyakan hal-hal umum mengenai Covid-19. Pengguna dapat melakukan vote terhadap pertanyaan. Pertanyaan dengan vote terbanyak akan tampil di posisi teratas. Data FAQ diambil dari database halaman web sebelumnya, yaitu pada https://zonahijau.herokuapp.com/faq/json/.

4. Pesan dari Pengguna (FUFU: From Us For Us)

Pada fitur ini, pengguna dapat mengirimkan pesan seputar Covid-19 seperti tips, pengalaman, ataupun pesan yang nantinya akan ditampilkan pada slider di section FUFU berupa card. Pengguna harus melakukan login terlebih dahulu jika ingin menambahkan FUFU. Halaman ini akan terintegrasi dengan web service django yaitu https://zonahijau.herokuapp.com/fufuJson/ untuk mendapatkan query yang berisi instance semua FUFU yang ada pada database untuk ditampilkan di slider.

5. My FUFU

Untuk mengakses fitur ini, pengguna harus melakukan login terlebih dahulu. Ketika menambahkan FUFU, pengguna dapat memilih untuk menggunakan username, custom, atau anonymous sebagai nama pengirim pesan dengan maksimal 10 karakter. Pesan yang dapat dikirimkan terdiri dari maksimal 150 karakter. Setelah pengguna menekan tombol ‘share’, pesan FUFU pengguna akan ditampilkan berupa card yang disusun dengan grid pada halaman FUFU yang dapat dilihat oleh seluruh pengguna dan halaman My FUFU. Setelahnya, pengguna dapat mengedit maupun menghapus pesannya.

6. Daftar Rumah Sakit atau Puskesmas Rujukan di Jabodetabek

Pada fitur ini, pengguna dapat mencari daftar rumah sakit rujukan Covid-19 di Jabodetabek yang disertai dengan alamat dan kontak dari rumah sakit rujukan tersebut. Selain itu, pengguna juga dapat melakukan pencarian daftar rumah sakit yang sesuai dengan wilayah yang diinginkan.
Data rumah sakit diambil dari database yang sama dengan halaman web yaitu pada https://zonahijau.herokuapp.com/rumahsakit/json/ sehingga hasil yang ditampilkan sinkron dengan yang ada pada web. 

7. Mitos atau Fakta

Pada fitur ini terdapat kuis yang membahas mengenai mitos dan fakta seputar Covid-19 yang beredar pada masyarakat. Di akhir kuis, terdapat pembahasan dari pernyataan-pernyataan mitos dan fakta yang sudah dikerjakan oleh pengguna sebelumnya. Setelah itu, skor pengguna akan dimasukkan dan ditampilkan pada leaderboard. Untuk mengakses fitur ini, pengguna harus melakukan login terlebih dahulu. Fitur pada app ini akan mengambil data seperti pertanyaan, jawaban, dan leaderboard dengan mengintegrasikan database dari web service pada web yang telah dibuat sebelumnya dengan app pada flutter.

8. Login dan Registrasi.

Modul ini berfungsi untuk melakukan registrasi dan login bagi para pengguna aplikasi ZonaHijau. Akan terdapat 2 halaman yang berisikan form untuk login dan register. Saat login, aplikasi ini akan terintegrasi dengan web service django untuk melakukan proses autentikasi sehingga pengguna bisa login ke akun mereka dan menggunakan berbagai macam modul yang mengharuskan user untuk login terlebih dahulu.

**Pembagian Kerja**

1. Fadhli Gazalba [2006525904]: Mitos atau Fakta
2. Kesya Aurelya [2006486046]: My FUFU dan add FUFU baru
3. Mochamad Thariq Zahir Abdillah [2006486185]: Infografis dan Video
4. Mohammad Bramantyo Putra Kusuma [2006520903]: List RS dan navbar
5. Pramudya Wibisono [2006526106]: Frequently Asked Question
6. Salma Karimah [2006529934]: Dashboard+feedback
7. Syahdan Putra Adriatama [2006486001]: login user, navbar, FUFU
