import 'package:flutter/material.dart';
import 'package:user/login.dart';

class SignUpForm extends StatefulWidget {

  final state = _SignUpFormState();

  @override
  State<StatefulWidget> createState() => state;
}

class _SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();
  final unameController = TextEditingController();
  final passController = TextEditingController();

  @override
  Widget build(BuildContext context) {

    @override
    void dispose() {
      unameController.dispose();
      passController.dispose();
      super.dispose();
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text("Sign Up ZonaHijau"),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width*0.80,
          child: Card(
            elevation: 0,
            child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "SIGN UP AN ACCOUNT",
                        style: Theme.of(context).textTheme.headline1,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        maxLength: 10,
                        decoration: InputDecoration(
                          counterText: "",
                          hintText: "Max 10 Characters",
                          labelText: "Username ...",
                          prefixIcon: const Icon(Icons.person),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Username can\'t be empty';
                          }
                          return null;
                        },
                        // controller: unameController,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: "Enter password",
                          prefixIcon: const Icon(Icons.lock),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Password can\'t be empty';
                          }
                          return null;
                        },
                        //  controller: passController,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: "Re-enter Password...",
                          prefixIcon: const Icon(Icons.lock),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Enter password confirmation';
                          }
                          if (value != passController.text) {
                            return 'Password missmatch';
                          }
                          return null;
                        },
                      ),
                    ),
                    ElevatedButton(
                      child: const Text(
                        "Sign Up",
                        style: TextStyle(color: Colors.white),
                      ),
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(Colors.teal[600]),
                        ),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          print("Sign in berhasil. Hi, " + unameController.text + "!");
                        }
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text("Already have an account?"),
                        TextButton(
                          onPressed: () {
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginForm()));
                          },
                          child: const Text("Login"))
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        )
      ),
    );
  }
}