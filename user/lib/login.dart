import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:user/cookieReq.dart';
import 'package:user/signup.dart';

class LoginForm extends StatefulWidget {

  final state = _LoginFormState();

  @override
  State<StatefulWidget> createState() => state;
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  String username = "";
  String password = "";
  final unameController = TextEditingController();
  final passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final request = context.watch<CookieRequest>();

    @override
    void dispose() {
      unameController.dispose();
      passController.dispose();
      super.dispose();
    }

    void login() async {
      final response = await request
          .login("http://zonahijau.herokuapp.com/loginFromFlutter/", {
        'username': username,
        'password': password,
      });
      if (request.loggedIn) {
        request.username = response["username"];
        request.userId = response["user_id"];
        Navigator.pop(context);
      } else {
        // Code here will run if the login failed (wrong username/password).
        setState(() {
          username = "";
          password = "";
        });
      }
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login ZonaHijau"),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width*0.80,
          child: Card(
            elevation: 0,
            child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "LOGIN",
                        style: Theme.of(context).textTheme.headline1,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        maxLength: 10,
                        decoration: InputDecoration(
                          counterText: "",
                          hintText: "Max 10 Characters",
                          labelText: "Username ...",
                          prefixIcon: const Icon(Icons.person),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Username can\'t be empty';
                          }
                          return null;
                        },
                        controller: unameController,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: "Enter password",
                          prefixIcon: const Icon(Icons.lock),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Password can\'t be empty';
                          }
                          return null;
                        },
                         controller: passController,
                      ),
                    ),
                    ElevatedButton(
                      child: const Text(
                        "Login",
                        style: TextStyle(color: Colors.white),
                      ),
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(Colors.teal[600]),
                        ),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          username = unameController.text;
                          password = passController.text;
                          login();
                          _formKey.currentState?.reset();
                          print("Sign in berhasil. Hi, " + unameController.text + "!");
                        }
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text("Don't have an account?"),
                        TextButton(
                          onPressed: () {
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SignUpForm()));
                          },
                          child: const Text("Sign Up"))
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        )
      ),
    );
  }
}