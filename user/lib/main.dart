import 'package:flutter/material.dart';
import 'package:user/login.dart';
import 'package:user/signup.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/parser.dart';
import 'package:provider/provider.dart';
import 'package:user/cookieReq.dart';

void main() {
  runApp( Provider(
    create: (_) {
          CookieRequest request = CookieRequest();
          return request;
        },
    child: MaterialApp(
      title: "ZonaHijau",
      home: UserDriver(),
      theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: const Color(0xFF7CC1AC),
          fontFamily: 'Sora',
          textTheme: const TextTheme(
            headline1: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Colors.black),
          )
        ),
    ),
  ));
}

class UserDriver extends StatefulWidget {

  final state = _UserDriverState();

  @override
  State<StatefulWidget> createState() => state;
}

class _UserDriverState extends State<UserDriver> {
  @override
  Widget build(BuildContext context) {

    CookieRequest cookie = context.read<CookieRequest>();
    String username = "belum login";

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              child: const Text('Login'),
              onPressed: () async {
                Navigator.push(context, MaterialPageRoute(builder: (context) => LoginForm()));
              },
            ),
            ElevatedButton(
              child: const Text('Sign Up'),
              onPressed: () async {
                await Navigator.push(context, MaterialPageRoute(builder: (context) => SignUpForm()));
              },
            ),
            Text(username),
            ElevatedButton(
              onPressed: () async {
                if (cookie.loggedIn) {
                  final response = await cookie
                      .logout("http://zonahijau.herokuapp.com/logoutFromFlutter/",
                    cookie.cookies,);
                  if (!cookie.loggedIn) {
                    cookie.username = "";
                    cookie.userId = -1;
                    print(response["message"]);
                  } else {
                    print("gagal logout");
                  }
                }
                else {
                  print("anda belum login");
                }
              } , 
              child: Text("Logout"))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(onPressed: () async {
        final response = await cookie
                      .get("http://zonahijau.herokuapp.com/getUsernameFromFlutter/");
        print(response["username"]);
        print(response["score"]);
        print(cookie.username);
        print(cookie.userId);
      },),

    );
  }
}