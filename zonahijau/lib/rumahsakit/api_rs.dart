import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:zonahijau/rumahsakit/rs_model.dart';

class FetchRumahSakit {
  var data = [];
  List<Rslist> result = [];

  Future<List<Rslist>> getRSlist(String filter) async {
    String fecthUrl =
        'https://zonahijau.herokuapp.com/rumahsakit/json/?filter=' + filter;
    // 'http://localhost:8000/rumahsakit/json/?filter=' + filter;
    var url = Uri.parse(fecthUrl);
    var response = await http.get(url);

    try {
      if (response.statusCode == 200) {
        data = json.decode(response.body);
        result = data.map((e) => Rslist.fromJson(e)).toList();
      } else {
        print("api error");
      }
    } on Exception catch (e) {
      print('error: $e');
    }

    return result;
  }
}
