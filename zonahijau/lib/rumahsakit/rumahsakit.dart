import 'package:flutter/material.dart';
import 'rs_model.dart';
import 'api_rs.dart';
import 'rs_form.dart';
import 'package:google_fonts/google_fonts.dart';

class rumahSakit extends StatefulWidget {
  const rumahSakit({Key? key}) : super(key: key);

  @override
  _rumahSakitState createState() => _rumahSakitState();
}

class _rumahSakitState extends State<rumahSakit> {
  final FetchRumahSakit _listRs = FetchRumahSakit();

  int selectedIndex = 0;
  final List<Daerah> _listDaerah = [
    Daerah('All'),
    Daerah('Jakarta'),
    Daerah('Bogor'),
    Daerah('Depok'),
    Daerah('Tangerang'),
    Daerah('Bekasi'),
  ];

  List<Widget> daerahChips() {
    List<Widget> chips = [];
    for (int i = 0; i < _listDaerah.length; i++) {
      Widget item = ChoiceChip(
        elevation: 4,
        pressElevation: 4,
        label: Text(_listDaerah[i].namaDaerah, style: GoogleFonts.sora()),
        labelStyle: const TextStyle(color: Colors.white),
        selected: selectedIndex == i,
        onSelected: (bool value) {
          setState(() {
            selectedIndex = i;
          });
        },
        backgroundColor: Colors.teal[200],
        selectedColor: Colors.teal[600],
      );
      chips.add(item);
      chips.add(const SizedBox(
        width: 4,
      ));
    }
    return chips;
  }

  @override
  Widget build(BuildContext context) {
    var _sliverbar = SliverAppBar(
      pinned: true,
      expandedHeight: 220,
      centerTitle: true,
      backgroundColor: Color(0xFF7CC1AC),
      flexibleSpace: FlexibleSpaceBar(
        title: Text('Rumah Sakit', style: GoogleFonts.sora()),
        // titlePadding: EdgeInsets.all(0),
        centerTitle: true,
        background: Padding(
          padding: EdgeInsets.fromLTRB(0, 5, 0, 25),
          child: Image(
              image: AssetImage('assets/rumahsakit/hospital-removebg.png')),
        ),
      ),
    );

    final _filterchoice = SliverToBoxAdapter(
        child: Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: SizedBox(
        height: 50,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: daerahChips(),
        ),
      ),
    ));

    return FutureBuilder<List<Rslist>>(
      future: (selectedIndex == 0)
          ? _listRs.getRSlist('all')
          : _listRs.getRSlist(_listDaerah[selectedIndex].namaDaerah),
      builder: (context, snapshot) {
        Widget _sliverData;
        if (!snapshot.hasData) {
          _sliverData =
              const SliverToBoxAdapter(child: CircularProgressIndicator());
        } else {
          var data = snapshot.data;
          _sliverData = SliverList(
              delegate:
                  SliverChildBuilderDelegate((BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.all(3),
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                elevation: 4,
                child: Column(
                  children: [
                    ListTile(
                      leading: Container(
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                          color: const Color(0xFFc77dff),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(4),
                          child: Center(
                              child: Image(
                            image: AssetImage(
                                'assets/rumahsakit/hospital-small.png'),
                          )),
                        ),
                      ),
                      title: Text(
                        '${data?[index].rumahSakit!.nama}',
                        style: GoogleFonts.sora(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text(
                        '${data?[index].rumahSakit!.nomorTelepon}',
                        style: GoogleFonts.sora(
                            color: Colors.black.withOpacity(0.8)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16),
                      child: Row(
                        children: [
                          Expanded(
                              child: Text(
                            '${data?[index].rumahSakit!.alamat}',
                            style: GoogleFonts.sora(
                                color: Colors.black.withOpacity(0.7)),
                            textAlign: TextAlign.left,
                          ))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          }, childCount: data?.length));
        }
        return SafeArea(
          child: Scaffold(
            body: CustomScrollView(
              slivers: <Widget>[_sliverbar, _filterchoice, _sliverData],
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const rsForm()));
              },
              backgroundColor: const Color(0xFFFDAF67),
              child: const Icon(Icons.add),
            ),
          ),
        );
      },
    );
  }
}
