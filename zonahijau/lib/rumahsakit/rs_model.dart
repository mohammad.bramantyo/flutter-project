class Rslist {
  String? model;
  int? pk;
  RumahSakit? rumahSakit;

  Rslist({this.model, this.pk, this.rumahSakit});

  Rslist.fromJson(Map<String, dynamic> json) {
    model = json['model'];
    pk = json['pk'];
    rumahSakit =
        json['fields'] != null ? RumahSakit.fromJson(json['fields']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['model'] = model;
    data['pk'] = pk;
    if (rumahSakit != null) {
      data['rumahsakit'] = rumahSakit!.toJson();
    }

    return data;
  }
}

class RumahSakit {
  String? nama;
  String? daerah;
  String? alamat;
  String? nomorTelepon;

  RumahSakit({
    this.nama,
    this.daerah,
    this.alamat,
    this.nomorTelepon,
  });

  RumahSakit.fromJson(Map<String, dynamic> json) {
    nama = json['nama'];
    daerah = json['daerah'];
    alamat = json['alamat'];
    nomorTelepon = json['nomor_telepon'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['nama'] = nama;
    data['daerah'] = daerah;
    data['alamat'] = alamat;
    data['nomor_telepon'] = nomorTelepon;

    return data;
  }
}

class Daerah {
  String namaDaerah;
  Daerah(this.namaDaerah);
}
