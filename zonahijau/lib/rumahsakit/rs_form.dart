import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;

class rsForm extends StatefulWidget {
  const rsForm({Key? key}) : super(key: key);

  @override
  _rsFormState createState() => _rsFormState();
}

class _rsFormState extends State<rsForm> {
  final _formKey = GlobalKey<FormState>();

  final List<String> _listDaerah = [
    'Jakarta',
    'Bogor',
    'Depok',
    'Tangerang',
    'Bekasi'
  ];
  String? _namaRS = "";
  String? _selectedDaerah = 'Jakarta';
  String? _alamat = "";
  String? _nomorTelepon = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tambah Rumah Sakit", style: GoogleFonts.sora()),
        backgroundColor: Color(0xFF7CC1AC),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: SizedBox(
            width: 380,
            height: 550,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              elevation: 8,
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(10, 10, 10, 5),
                          child: Text(
                            "Tambah Rumah Sakit",
                            style: GoogleFonts.sora(
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                                color: Colors.black),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                          child: TextFormField(
                            decoration: InputDecoration(
                              hintText: "contoh: RSUD Pasar Minggu",
                              labelText: "Nama Rumah Sakit",
                              fillColor: Colors.white,
                              filled: true,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0)),
                              focusColor: const Color(0xFFDEB887),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Mohon isi field ini';
                              }
                              return null;
                            },
                            onChanged: (value) {
                              setState(() {
                                _namaRS = value;
                              });
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                          child: DropdownButtonFormField(
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                            ),
                            // value: _selectedDaerah,
                            hint: Text('Daerah'),
                            items: _listDaerah.map((String item) {
                              return DropdownMenuItem(
                                child: Text('$item'),
                                value: item,
                              );
                            }).toList(),
                            onChanged: (String? value) {
                              setState(() {
                                _selectedDaerah = value;
                              });
                            },
                            validator: (value) {
                              if (value == null) {
                                return 'Mohon isi field ini';
                              }
                              return null;
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                          child: TextFormField(
                            keyboardType: TextInputType.multiline,
                            maxLines: 3,
                            decoration: InputDecoration(
                              hintText: "contoh: Jl.H.Nangka No.666",
                              labelText: "Alamat",
                              fillColor: Colors.white,
                              filled: true,
                              border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(5.0)),
                              focusColor: const Color(0xFFDEB887),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Mohon isi field ini';
                              }
                              return null;
                            },
                            onChanged: (value) {
                              setState(() {
                                _alamat = value;
                              });
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                          child: TextFormField(
                            maxLength: 16,
                            decoration: InputDecoration(
                              hintText: "contoh: 021-8989-9999",
                              labelText: "Nomor Telepon",
                              fillColor: Colors.white,
                              filled: true,
                              border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(5.0)),
                              focusColor: const Color(0xFFDEB887),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Mohon isi field ini';
                              }
                              return null;
                            },
                            onChanged: (value) {
                              setState(() {
                                _nomorTelepon = value;
                              });
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: SizedBox(
                            width: 170,
                            height: 40,
                            child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: Color.fromRGBO(253, 175, 103, 1),
                                  elevation: 5,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                ),
                                onPressed: () async {
                                  String postUrl =
                                      "https://zonahijau.herokuapp.com/rumahsakit/add_from_flutter";

                                  var url = Uri.parse(postUrl);

                                  if (_formKey.currentState!.validate()) {
                                    final response = await http.post(url,
                                        body: json.encode({
                                          'nama': _namaRS,
                                          'daerah': _selectedDaerah,
                                          'alamat': _alamat,
                                          'nomor_telepon': _nomorTelepon
                                        }));
                                    _formKey.currentState?.reset();
                                    Navigator.pop(context);
                                  }
                                },
                                child: Text('Tambahkan',
                                    style: GoogleFonts.sora())),
                          ),
                        )
                      ],
                    )),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
