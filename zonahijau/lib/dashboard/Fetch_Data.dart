import 'dart:convert';
import 'package:http/http.dart' as http;
import 'Data.dart';

class Fetch_Data {
  late Data data;
  // Data res = null;

  Future<Data> getData() async {
    String fecthUrl =
        "https://apicovid19indonesia-v2.vercel.app/api/indonesia/";
    var url = Uri.parse(fecthUrl);
    Map<String, String> headers = {
      "access-control-allow-origin": "*",
      "Access-Control-Allow-Methods": "GET"
    };

    var response = await http.get(url, headers: headers);

    try {
      if (response.statusCode == 200) {
        Map<String, dynamic> dataJson = json.decode(response.body);
        data = Data(
            positif: dataJson['positif'],
            dirawat: dataJson['dirawat'],
            sembuh: dataJson['sembuh'],
            meninggal: dataJson['meninggal']);
      } else {
        print("api error");
      }
    } on Exception catch (e) {
      print('error: $e');
    }
    return data;
  }
}
