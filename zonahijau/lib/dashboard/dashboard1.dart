import 'dart:async';
import 'dart:convert';
import 'package:zonahijau/faq/faq_model.dart';

import 'Data.dart';
import 'Fetch_Data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:zonahijau/mainDrawer.dart';

import 'package:zonahijau/faq/faq_home.dart';
import 'package:zonahijau/rumahsakit/rumahsakit.dart';
import 'package:zonahijau/fufu/fufu_index.dart';
import 'package:zonahijau/dashboard/dashboard1.dart';
import 'package:zonahijau/videoinfog/infogpage.dart';
import 'package:zonahijau/mitosfakta/mitosfakta_home.dart';

/* SOME IDEAS INSPIRED BY:
    1. Get API data: https://www.youtube.com/watch?v=MIY5h00UpJ4&t=711s
    2. UI Design Ideas:
        - 
*/
class DashboardApp extends StatefulWidget {
  @override
  _DashboardAppState createState() => _DashboardAppState();
}

class _DashboardAppState extends State<DashboardApp> {
  Fetch_Data aData = Fetch_Data();
  Data covidData = new Data();

  late Future<List<dynamic>> futureData;
  final _formKey = GlobalKey<FormState>();

  TextEditingController judul = TextEditingController();
  TextEditingController isi = TextEditingController();

  /* Fungsi untuk form */
  void allInfo() {
    AlertDialog popUp = AlertDialog(
      content: Container(
        height: 300.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Judul Aduan: ",
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            Text("${judul.text}"),
            Padding(padding: EdgeInsets.only(top: 30.0)),
            Text(
              "Pesan Aduan: ",
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            Text("${isi.text}")
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (_) => popUp);
  }

  // void initState() {
  //   super.initState();
  //   futureData = aData.getData();
  // }

  @override
  void initState() {
    aData.getData().then((value) {
      covidData = value;
      //print(value.dirawat);
      setState(() {});
    });
  }

  /* Tampilan UI dashboard */
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        /* AppBar dari dashboard ZonaHijau */
        appBar: AppBar(
          title: Text(
            "This is Dashboard",
            style: GoogleFonts.poppins(
              fontWeight: FontWeight.bold,
              backgroundColor: const Color(0xFF7CC1AC),
            ),
          ),
        ),

        /* Body: Tampilan isi dashboard ZonaHijau */
        body: SingleChildScrollView(
          child: Container(
            padding:
                const EdgeInsets.only(top: 50, left: 30, right: 30, bottom: 40),
            child: Column(
              children: [
                /* Image ZonaHijau */
                Container(
                  height: 330,
                  child: Image(image: AssetImage('assets/dashboard/mask3.png')),
                ),

                /* Box kenalan ZonaHijau */
                Container(
                  height: 250,
                  /* Set up radius edges */
                  decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Colors.teal.withOpacity(0.8),
                      const Color(0xFF7CC1AC),
                      Colors.tealAccent.withOpacity(0.9),
                    ]),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                      topRight: Radius.circular(80),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        spreadRadius: 3,
                        blurRadius: 10,
                        offset: Offset(0, 3),
                      )
                    ],
                  ),
                  child: Container(
                    padding:
                        const EdgeInsets.only(top: 20, left: 20, right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Zona Hijau",
                          style: GoogleFonts.poppins(
                            fontWeight: FontWeight.bold,
                            fontSize: 40,
                            color: const Color(0xFF244D2D),
                          ),
                        ),

                        /* Belum textalignment */
                        Text(
                          'Zona Hijau adalah suatu web yang didesain khusus untuk membantu masyarakat Indonesia mengetahui kondisi terkait persebaran Covid-19 serta meningkatkan pemahaman pengguna terhadap virus Covid-19.',
                          style: GoogleFonts.poppins(
                            fontSize: 15,
                            height: 1.5,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                /* Data Covid-19 */
                Center(
                  child: Text(
                    "Data COVID-19 di Indonesia",
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.bold,
                      fontSize: 40,
                      color: const Color(0xFF244D2D),
                    ),
                  ),
                ),

                /* Data API Covid 19 */
                Container(
                  margin: const EdgeInsets.only(top: 20),
                  height: 100,
                  child: Row(
                    children: [
                      /* SEMBUH */
                      Container(
                        margin: const EdgeInsets.only(right: 20),
                        padding: const EdgeInsets.only(right: 10),
                        height: 100,
                        width: (MediaQuery.of(context).size.width * 0.5) - 50,
                        decoration: BoxDecoration(
                          color: const Color(0xFF277066),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                            topRight: Radius.circular(10),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.2),
                              spreadRadius: 5,
                              blurRadius: 10,
                              offset: Offset(0, 3),
                            )
                          ],
                        ),

                        /* SEMBUH */
                        child: Center(
                          child: Container(
                            padding: const EdgeInsets.only(
                                top: 20, left: 20, right: 20),
                            child: Column(
                              children: [
                                Text(
                                  "Sembuh",
                                  style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Colors.white,
                                  ),
                                ),

                                /* Belum textalignment */
                                Text(
                                  '${covidData.sembuh}',
                                  style: GoogleFonts.poppins(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    height: 1.5,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),

                      /* POSITIF */
                      Container(
                        margin: const EdgeInsets.only(left: 20),
                        padding: const EdgeInsets.only(left: 10),
                        height: 100,
                        width: (MediaQuery.of(context).size.width * 0.5) - 50,
                        decoration: BoxDecoration(
                          color: const Color(0xFF277066),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                            topRight: Radius.circular(10),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.2),
                              spreadRadius: 5,
                              blurRadius: 10,
                              offset: Offset(0, 3),
                            )
                          ],
                        ),
                        child: Center(
                          child: Container(
                            padding: const EdgeInsets.only(
                                top: 20, left: 20, right: 20),
                            child: Column(
                              children: [
                                Text(
                                  "Positif",
                                  style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Colors.white,
                                  ),
                                ),

                                /* Belum textalignment */
                                Text(
                                  '${covidData.positif}',
                                  style: GoogleFonts.poppins(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    height: 1.5,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                /* DATA API 2 */
                Container(
//              width: MediaQuery.of(context).size.width,
                  height: 100,
                  margin: const EdgeInsets.only(top: 20),

                  child: Row(
                    children: [
                      /* DIRAWAT */
                      Container(
                        margin: const EdgeInsets.only(right: 20),
                        padding: const EdgeInsets.only(right: 10),
                        height: 100,
                        width: (MediaQuery.of(context).size.width * 0.5) - 50,
                        decoration: BoxDecoration(
                          color: const Color(0xFF277066),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                            topRight: Radius.circular(10),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.2),
                              spreadRadius: 5,
                              blurRadius: 10,
                              offset: Offset(0, 3),
                            )
                          ],
                        ),

                        /* Dirawat */
                        child: Center(
                          child: Container(
                            padding: const EdgeInsets.only(
                                top: 20, left: 20, right: 20),
                            child: Column(
                              children: [
                                Text(
                                  "Dirawat",
                                  style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Colors.white,
                                  ),
                                ),

                                /* Belum textalignment */
                                Text(
                                  '${covidData.dirawat}',
                                  style: GoogleFonts.poppins(
                                    fontSize: 15,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    height: 1.5,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),

                      /* Meninggal */
                      Container(
                        margin: const EdgeInsets.only(left: 20),
                        padding: const EdgeInsets.only(left: 10),
                        height: 100,
                        width: (MediaQuery.of(context).size.width * 0.5) - 50,
                        decoration: BoxDecoration(
                          color: const Color(0xFF277066),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                            topRight: Radius.circular(10),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.2),
                              spreadRadius: 5,
                              blurRadius: 10,
                              offset: Offset(0, 3),
                            )
                          ],
                        ),
                        child: Center(
                          child: Container(
                            padding: const EdgeInsets.only(
                                top: 20, left: 20, right: 20),
                            child: Column(
                              children: [
                                Text(
                                  "Meninggal",
                                  style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),

                                /* Belum textalignment */
                                Text(
                                  '${covidData.meninggal}',
                                  style: GoogleFonts.poppins(
                                    fontSize: 15,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    height: 1.5,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                /* FEEDBACK FORM */
                Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  padding: EdgeInsets.all(10.0),
                  child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(padding: EdgeInsets.all(30.0)),
                        Center(
                          child: Container(
                            child: Image(
                                image: AssetImage(
                                    'assets/dashboard/feedback.png')),
                          ),
                        ),
                        Center(
                          child: Text(
                            "GIVE YOUR FEEDBACK!",
                            style: GoogleFonts.poppins(
                              fontWeight: FontWeight.bold,
                              fontSize: 50.0,
                              color: const Color(0xFF244D2D),
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.all(30.0)),
                        Text(
                          "Judul: ",
                          style: GoogleFonts.poppins(
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0,
                          ),
                        ),
                        TextField(
                          controller: judul,
                          decoration: InputDecoration(
                              hintText: "Judul",
                              labelText: "Judul",
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0))),
                        ),
                        Padding(padding: EdgeInsets.all(30.0)),
                        Text(
                          "Pesan: ",
                          style: GoogleFonts.poppins(
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0,
                          ),
                        ),
                        TextField(
                          controller: isi,
                          maxLines: 5,
                          decoration: InputDecoration(
                              hintText: "Pesan",
                              labelText: "Pesan",
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0))),
                        ),
                        Padding(padding: EdgeInsets.all(30.0)),
                        RaisedButton(
                          child: Text(
                            "Submit",
                            style: GoogleFonts.poppins(
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                            ),
                          ),
                          color: Colors.teal,
                          onPressed: () {
                            allInfo();
                          },
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: const Color(0xFF7CC1AC),
                ),
                child: Text(
                  'ZONA HIJAU APP',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.design_services),
                title: Text("INFOGRAFIS"),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Videoinfog(),
                      ));
                },
              ),
              ListTile(
                leading: Icon(Icons.quiz),
                title: Text('MITOS / FAKTA'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MitosFaktaHome(),
                      ));
                },
              ),
              ListTile(
                leading: Icon(Icons.medical_services),
                title: Text('LIST RS'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => rumahSakit(),
                      ));
                },
              ),
              ListTile(
                leading: Icon(Icons.comment),
                title: Text('FUFU'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Fufu(),
                      ));
                },
              ),
              ListTile(
                leading: Icon(Icons.question_answer),
                title: Text('FAQ'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => FAQApp(),
                      ));
                },
              ),
            ],
          ),
        ));
  }
}
