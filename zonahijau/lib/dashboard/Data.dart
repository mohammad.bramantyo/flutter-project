class Data {
  int? positif;
  int? dirawat;
  int? sembuh;
  int? meninggal;
  String? lastUpdate;

  Data(
      {this.positif,
      this.dirawat,
      this.sembuh,
      this.meninggal,
      this.lastUpdate});

  Data.fromJson(Map<String, dynamic> json) {
    positif = json['positif'];
    dirawat = json['dirawat'];
    sembuh = json['sembuh'];
    meninggal = json['meninggal'];
    lastUpdate = json['lastUpdate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['positif'] = this.positif;
    data['dirawat'] = this.dirawat;
    data['sembuh'] = this.sembuh;
    data['meninggal'] = this.meninggal;
    data['lastUpdate'] = this.lastUpdate;
    return data;
  }
}
