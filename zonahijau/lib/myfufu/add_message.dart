// import 'package:flutter/material.dart';
// import 'homepage.dart';
// import 'package:http/http.dart' as http;
// import 'dart:convert';
// import 'package:google_fonts/google_fonts.dart';

// class AddMessage extends StatefulWidget {
//   @override
//   State<AddMessage> createState() => _AddMessageState();
// }

// class _AddMessageState extends State<AddMessage> {
//   final _formKey = GlobalKey<FormState>();

//   final TextEditingController _fromController = TextEditingController(text: '');
//   final TextEditingController _messageController = TextEditingController();

//   Future saveProduct() async {
//     final response =
//         await http.post(Uri.parse("http://127.0.0.1:8000/api/products"), body: {
//       "from": _fromController.text,
//       "message": _messageController.text,
//     });

//     print(response.body);

//     return json.decode(response.body);
//   }

//   int _value = 1;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           backgroundColor: const Color(0xff7DC1AC),
//           title: const Text("Add Message"),
//         ),
//         body: Container(
//           decoration: const BoxDecoration(
//               image: DecorationImage(
//             image: AssetImage(
//               "assets/background.png",
//             ),
//             fit: BoxFit.cover,
//           )),
//           child: Form(
//             key: _formKey,
//             child: Column(
//               children: [
//                 const SizedBox(
//                   height: 40,
//                 ),
//                 Padding(
//                   padding:
//                       const EdgeInsets.only(top: 10.0, left: 15, right: 15),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Text(
//                         'Write down your message:',
//                         style: GoogleFonts.poppins(
//                           color: const Color(0XFF17161B),
//                           fontSize: 15,
//                           fontWeight: FontWeight.w500,
//                         ),
//                       ),
//                       const SizedBox(
//                         height: 20,
//                       ),
//                       TextFormField(
//                         maxLength: 10,
//                         controller: _fromController,
//                         decoration: InputDecoration(
//                           focusedBorder: OutlineInputBorder(
//                             borderRadius: BorderRadius.circular(25.0),
//                             borderSide: const BorderSide(
//                               color: Colors.black,
//                             ),
//                           ),
//                           enabledBorder: OutlineInputBorder(
//                             borderRadius: BorderRadius.circular(25.0),
//                             borderSide: const BorderSide(
//                               color: Colors.black,
//                             ),
//                           ),
//                           labelText: "Write your name",
//                           labelStyle: const TextStyle(
//                             color: Colors.black,
//                             fontSize: 13,
//                           ),
//                           hintText: "Name",
//                           hintStyle: const TextStyle(
//                             color: Colors.grey,
//                           ),
//                         ),
//                         validator: (value) {
//                           if (value == null || value.isEmpty) {
//                             return "Please enter your name";
//                           }
//                           return null;
//                         },
//                       ),
//                       Row(
//                         children: [
//                           Radio(
//                               value: 1,
//                               groupValue: _value,
//                               onChanged: (value) {
//                                 setState(() {
//                                   // _value = value;
//                                   _fromController.text = "";
//                                 });
//                               }),
//                           const Text('Custom'),
//                           Radio(
//                               value: 2,
//                               groupValue: _value,
//                               onChanged: (value) {
//                                 setState(() {
//                                   // _value = value;
//                                   _fromController.text = "Anonymous";
//                                 });
//                               }),
//                           const Text('Anonymous'),
//                         ],
//                       )
//                     ],
//                   ),
//                 ),
//                 const SizedBox(
//                   height: 20,
//                 ),
//                 Padding(
//                   padding:
//                       const EdgeInsets.only(top: 10.0, left: 15, right: 15),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       TextFormField(
//                         maxLength: 150,
//                         controller: _messageController,
//                         decoration: InputDecoration(
//                           focusedBorder: OutlineInputBorder(
//                             borderRadius: BorderRadius.circular(25.0),
//                             borderSide: const BorderSide(
//                               color: Colors.black,
//                             ),
//                           ),
//                           enabledBorder: OutlineInputBorder(
//                             borderRadius: BorderRadius.circular(25.0),
//                             borderSide: const BorderSide(
//                               color: Colors.black,
//                             ),
//                           ),
//                           labelText: "Write your message ...",
//                           labelStyle: const TextStyle(
//                             color: Colors.black,
//                             fontSize: 13,
//                           ),
//                           hintText: 'Message',
//                           hintStyle: const TextStyle(
//                             color: Colors.grey,
//                           ),
//                         ),
//                         validator: (value) {
//                           if (value == null || value.isEmpty) {
//                             return "Please enter the message";
//                           }
//                           return null;
//                         },
//                       ),
//                     ],
//                   ),
//                 ),
//                 const SizedBox(height: 20),
//                 ElevatedButton(
//                   style: ElevatedButton.styleFrom(
//                     padding: const EdgeInsets.only(
//                         top: 10, right: 30, left: 30, bottom: 10),
//                     primary: const Color(0xff7DC1AC),
//                   ),
//                   onPressed: () {
//                     if (_formKey.currentState!.validate()) {
//                       saveProduct().then((value) {
//                         Navigator.push(
//                             context,
//                             MaterialPageRoute(
//                                 builder: (context) => const HomePage()));
//                         ScaffoldMessenger.of(context).showSnackBar(
//                           const SnackBar(
//                             content: Text("Message successfully added!"),
//                           ),
//                         );
//                       });
//                     }
//                   },
//                   child: const Text("Save"),
//                 ),
//               ],
//             ),
//           ),
//         ));
//   }
// }
