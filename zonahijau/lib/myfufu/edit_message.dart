// import 'package:flutter/material.dart';
// import 'homepage.dart';
// import 'package:http/http.dart' as http;
// import 'dart:convert';
// import 'package:google_fonts/google_fonts.dart';

// class EditMessage extends StatelessWidget {
//   final Map product;

//   EditMessage({required this.product});
//   final _formKey = GlobalKey<FormState>();
//   final TextEditingController _fromController = TextEditingController();
//   final TextEditingController _messageController = TextEditingController();

//   Future updateProduct() async {
//     final response = await http.put(
//         Uri.parse(
//             "http://127.0.0.1:8000/api/products/" + product['id'].toString()),
//         body: {
//           "from": _fromController.text,
//           "message": _messageController.text,
//         });

//     print(response.body);

//     return json.decode(response.body);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           backgroundColor: const Color(0xff7DC1AC),
//           title: const Text("Edit Message"),
//         ),
//         body: Container(
//           decoration: const BoxDecoration(
//               image: DecorationImage(
//             image: AssetImage(
//               "assets/background.png",
//             ),
//             fit: BoxFit.cover,
//           )),
//           child: Form(
//             key: _formKey,
//             child: Padding(
//               padding: const EdgeInsets.all(10.0),
//               child: Column(
//                 children: [
//                   Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       const SizedBox(
//                         height: 40,
//                       ),
//                       Text(
//                         'Edit your message:',
//                         style: GoogleFonts.poppins(
//                           color: const Color(0XFF17161B),
//                           fontSize: 15,
//                           fontWeight: FontWeight.w500,
//                         ),
//                       ),
//                       const SizedBox(
//                         height: 20,
//                       ),
//                       TextFormField(
//                         maxLength: 10,
//                         controller: _fromController..text = product['from'],
//                         decoration: InputDecoration(
//                           focusedBorder: OutlineInputBorder(
//                             borderRadius: BorderRadius.circular(25.0),
//                             borderSide: const BorderSide(
//                               color: Colors.black,
//                             ),
//                           ),
//                           enabledBorder: OutlineInputBorder(
//                             borderRadius: BorderRadius.circular(25.0),
//                             borderSide: const BorderSide(
//                               color: Colors.black,
//                             ),
//                           ),
//                           labelText: "From",
//                         ),
//                         validator: (value) {
//                           if (value == null || value.isEmpty) {
//                             return "Please enter product name";
//                           }
//                           return null;
//                         },
//                       ),
//                     ],
//                   ),
//                   const SizedBox(
//                     height: 30,
//                   ),
//                   TextFormField(
//                     maxLength: 150,
//                     controller: _messageController..text = product['message'],
//                     decoration: InputDecoration(
//                       focusedBorder: OutlineInputBorder(
//                         borderRadius: BorderRadius.circular(25.0),
//                         borderSide: const BorderSide(
//                           color: Colors.black,
//                         ),
//                       ),
//                       enabledBorder: OutlineInputBorder(
//                         borderRadius: BorderRadius.circular(25.0),
//                         borderSide: const BorderSide(
//                           color: Colors.black,
//                         ),
//                       ),
//                       labelText: "Message",
//                     ),
//                     validator: (value) {
//                       if (value == null || value.isEmpty) {
//                         return "Please enter the description";
//                       }
//                       return null;
//                     },
//                   ),
//                   const SizedBox(height: 20),
//                   ElevatedButton(
//                     style: ElevatedButton.styleFrom(
//                       elevation: 5,
//                       padding: const EdgeInsets.only(
//                           top: 10, right: 30, left: 30, bottom: 10),
//                       primary: const Color(0xff7DC1AC),
//                     ),
//                     onPressed: () {
//                       if (_formKey.currentState!.validate()) {
//                         updateProduct().then(
//                           (value) {
//                             Navigator.push(
//                                 context,
//                                 MaterialPageRoute(
//                                     builder: (context) => const HomePage()));
//                             ScaffoldMessenger.of(context).showSnackBar(
//                               const SnackBar(
//                                 content: Text("Product successfully update!"),
//                               ),
//                             );
//                           },
//                         );
//                       }
//                     },
//                     child: const Text(
//                       "Update",
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ));
//   }
// }
