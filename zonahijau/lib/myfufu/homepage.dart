// import 'dart:convert';

// import 'package:flutter/material.dart';
// import 'add_message.dart';
// import 'edit_message.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:http/http.dart' as http;

// class HomePage extends StatefulWidget {
//   const HomePage({
//     Key? key,
//   }) : super(key: key);

//   @override
//   State<HomePage> createState() => _HomePageState();
// }

// class _HomePageState extends State<HomePage> {
//   final String url = 'http://127.0.0.1:8000/api/products';

//   Future getProducts() async {
//     var response = await http.get(Uri.parse(url));
//     print(json.decode(response.body));
//     return json.decode(response.body);
//   }

//   Future deleteProduct(String productId) async {
//     String url = 'http://127.0.0.1:8000/api/products/' + productId;

//     var response = await http.delete(Uri.parse(url));
//     return json.decode(response.body);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         backgroundColor: const Color(0xffffffff),
//         floatingActionButton: FloatingActionButton(
//           backgroundColor: const Color(0xff7DC1AC),
//           onPressed: () {
//             Navigator.push(
//                 context, MaterialPageRoute(builder: (context) => AddMessage()));
//           },
//           child: const Icon(
//             Icons.add,
//             size: 20,
//           ),
//         ),
//         appBar: AppBar(
//           automaticallyImplyLeading: false,
//           backgroundColor: const Color(0xff7DC1AC),
//           title: const Text('MY FUFU'),
//         ),
//         body: Container(
//           decoration: const BoxDecoration(
//               image: DecorationImage(
//             image: AssetImage(
//               "assets/background.png",
//             ),
//             fit: BoxFit.cover,
//           )),
//           child: FutureBuilder(
//             future: getProducts(),
//             builder: (context, snapshot) {
//               if (snapshot.hasData) {
//                 var data = snapshot.data;
//                 return Column(
//                   children: [
//                     const SizedBox(
//                       height: 25,
//                     ),
//                     Text(
//                       'Welcome to MY FUFU',
//                       style: GoogleFonts.poppins(
//                         color: const Color(0XFF17161B),
//                         fontSize: 20,
//                         fontWeight: FontWeight.w400,
//                       ),
//                     ),
//                     Text(
//                       'Add, Edit, and Delete Your Message',
//                       style: GoogleFonts.poppins(
//                         color: const Color(0XFF17161B),
//                         fontSize: 15,
//                         fontWeight: FontWeight.w200,
//                       ),
//                     ),
//                     const SizedBox(
//                       height: 10,
//                     ),
//                     Expanded(
//                       child: ListView.builder(
//                           itemCount: 2,
//                           itemBuilder: (context, index) {
//                             return Container(
//                               padding: const EdgeInsets.only(
//                                 top: 15,
//                                 left: 15,
//                                 right: 15,
//                               ),
//                               height: 215,
//                               child: Card(
//                                 shape: RoundedRectangleBorder(
//                                     borderRadius: BorderRadius.circular(24)),
//                                 shadowColor: const Color(0xff7DC1AC),
//                                 color: const Color(0xffF4F4F4),
//                                 elevation: 13,
//                                 child: Container(
//                                   padding: const EdgeInsets.all(25.0),
//                                   child: Column(
//                                     mainAxisAlignment:
//                                         MainAxisAlignment.spaceAround,
//                                     children: [
//                                       Align(
//                                         alignment: Alignment.topLeft,
//                                         child: Row(
//                                           children: [
//                                             Text(
//                                               "From:",
//                                               style: GoogleFonts.poppins(
//                                                 color: const Color(0XFF17161B),
//                                                 fontSize: 16,
//                                                 fontWeight: FontWeight.bold,
//                                                 decoration:
//                                                     TextDecoration.underline,
//                                               ),
//                                             ),
//                                             const SizedBox(
//                                               width: 10,
//                                             ),
//                                             // Text(
//                                             //   snapshot.data['data'][index]
//                                             //       ['from'],
//                                             //   style: GoogleFonts.poppins(
//                                             //     color: const Color(0XFF17161B),
//                                             //     fontSize: 16,
//                                             //     fontWeight: FontWeight.w400,
//                                             //   ),
//                                             // ),
//                                           ],
//                                         ),
//                                       ),
//                                       Align(
//                                         alignment: Alignment.topLeft,
//                                         // child: Text(
//                                         //   snapshot.data['data'][index]
//                                         //       ['message'],
//                                         //   style: GoogleFonts.poppins(
//                                         //     color: const Color(0XFF17161B),
//                                         //     fontSize: 13,
//                                         //     fontWeight: FontWeight.w400,
//                                         //   ),
//                                         // ),
//                                       ),
//                                       Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.end,
//                                         children: [
//                                           GestureDetector(
//                                             onTap: () {
//                                               // Navigator.push(
//                                               //     context,
//                                               //     MaterialPageRoute(
//                                               //         builder: (context) =>
//                                               //             EditMessage(
//                                               //                 product: snapshot
//                                               //                             .data[
//                                               //                         'data']
//                                               //                     [index])));
//                                             },
//                                             child: Image.asset(
//                                               'assets/edit_icon.png',
//                                               height: 20,
//                                             ),
//                                           ),
//                                           const SizedBox(
//                                             width: 13,
//                                           ),
//                                           GestureDetector(
//                                             onTap: () {
//                                               // deleteProduct(snapshot
//                                               //         .data!['data'][index]['id']
//                                               //         .toString())
//                                               //     .then((value) {
//                                               //   setState(() {});
//                                               //   ScaffoldMessenger.of(context)
//                                               //       .showSnackBar(
//                                               //     const SnackBar(
//                                               //       content: Text(
//                                               //           "Message successfully deleted!"),
//                                               //     ),
//                                               //   );
//                                               // });
//                                             },
//                                             child: Image.asset(
//                                               'assets/delete_icon.png',
//                                               height: 20,
//                                             ),
//                                           ),
//                                         ],
//                                       ),
//                                     ],
//                                   ),
//                                 ),
//                               ),
//                             );
//                           }),
//                     ),
//                     const SizedBox(
//                       height: 50,
//                     ),
//                   ],
//                 );
//               } else {
//                 return const Text("You haven't add any FUFU",
//                     style: TextStyle(
//                         color: Color(0xff7DC1AC), height: 50, fontSize: 40));
//               }
//             },
//           ),
//         ));
//   }
// }
