class FufuModel {
  String? model;
  int? pk;
  Fields? fields;

  FufuModel({this.model, this.pk, this.fields});

  FufuModel.fromJson(Map<String, dynamic> json) {
    model = json['model'];
    pk = json['pk'];
    fields =
        json['fields'] != null ? new Fields.fromJson(json['fields']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['model'] = model;
    data['pk'] = pk;
    if (fields != null) {
      data['fields'] = fields!.toJson();
    }
    return data;
  }
}

class Fields {
  String? from;
  String? message;
  int? owner;

  Fields({this.from, this.message, this.owner});

  Fields.fromJson(Map<String, dynamic> json) {
    from = json['From'];
    message = json['Message'];
    owner = int.parse(json['Owner']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['From'] = from;
    data['Message'] = message;
    data['Owner'] = owner;
    return data;
  }
}
