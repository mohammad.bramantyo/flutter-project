class Video {
  String? model;
  int? pk;
  Fields? fields;

  Video({this.model, this.pk, this.fields});

  Video.fromJson(Map<String, dynamic> json) {
    model = json['model'];
    pk = json['pk'];
    fields =
        json['fields'] != null ? new Fields.fromJson(json['fields']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['model'] = this.model;
    data['pk'] = this.pk;
    if (this.fields != null) {
      data['fields'] = this.fields!.toJson();
    }
    return data;
  }
}

class Fields {
  String? tittle;
  String? added;
  String? url;

  Fields({this.tittle, this.added, this.url});

  Fields.fromJson(Map<String, dynamic> json) {
    tittle = json['tittle'];
    added = json['added'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tittle'] = this.tittle;
    data['added'] = this.added;
    data['url'] = this.url;
    return data;
  }
}
