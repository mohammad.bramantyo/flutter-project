import 'package:flutter/material.dart';
import 'video.dart';
import 'videoview.dart';
import 'videoform.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class Videoinfog extends StatefulWidget {
  const Videoinfog({Key? key}) : super(key: key);

  @override
  _VideoinfogState createState() => _VideoinfogState();
}

class _VideoinfogState extends State<Videoinfog> {
  final FetchVideo vid = FetchVideo();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Video dan Infografis",
          style: const TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      backgroundColor: Color.fromARGB(255, 150, 190, 171),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: Container(
              child: Column(
                children: [
                  Container(
                    child: Image(
                        image: AssetImage('assets/videoinfog/PakaiMasker.png')),
                    height: 250,
                    width: 250,
                    margin: EdgeInsets.only(bottom: 20),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 15),
                    child: Text(
                      "Memakai Masker",
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Text(
                    "Banyak yang menggunakan masker kain untuk mencegah infeksi virus Corona, padahal masker tersebut belum tentu efektif. Secara umum, ada dua tipe masker yang bisa Anda digunakan untuk mencegah penularan virus Corona, yaitu masker bedah dan masker N95.",
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 15,
                      letterSpacing: 1.5,
                      height: 1.5,
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
            child: Container(
              child: Column(
                children: [
                  Container(
                    child: Image(
                        image: AssetImage('assets/videoinfog/Vaksinasi.png')),
                    height: 250,
                    width: 250,
                    margin: EdgeInsets.only(bottom: 20),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 15),
                    child: Text(
                      "Melakukan Vaksinasi",
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Text(
                    "Pemberian vaksin ini merupakan solusi yang dianggap paling tepat untuk mengurangi dan memutus rantai penularan Covid-19. Vaksinasi bertujuan untuk memberikan kekebalan spesifik terhadap suatu penyakit tertentu sehingga jika suatu saat terpapar penyakit tersebut maka hanya akan mengalami gejala yang ringan.",
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 15,
                      letterSpacing: 1.5,
                      height: 1.5,
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 10.0),
            child: Container(
              child: Column(
                children: [
                  Container(
                    child: Image(
                        image: AssetImage('assets/videoinfog/CuciTangan.png')),
                    height: 280,
                    width: 280,
                    // margin: EdgeInsets.only(bottom: 20),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 15),
                    child: Text(
                      "Mencuci Tangan",
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Text(
                    "Mencuci tangan dengan benar atau menggunakan hand sanitizer adalah cara paling sederhana namun efektif untuk mencegah penyebaran virus 2019-nCoV. Cucilah tangan dengan air mengalir dan sabun, setidaknya selama 20 detik atau gunakan hand sanitizer jika sulit untuk menemukan air dan sabun.",
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 15,
                      letterSpacing: 1.5,
                      height: 1.5,
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: Container(
              child: Column(
                children: [
                  Container(
                    child: Image(
                        image:
                            AssetImage('assets/videoinfog/dayatahantubuh.png')),
                    height: 300,
                    width: 300,
                    margin: EdgeInsets.only(bottom: 20),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 15),
                    child: Text(
                      "Menjaga Daya Tahan Tubuh",
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Text(
                    "Daya tahan tubuh yang kuat dapat mencegah munculnya berbagai macam penyakit. Untuk menjaga dan meningkatkan daya tahan tubuh, Anda disarankan untuk mengonsumsi makanan sehat, minum vitamin, rutin berolahraga, dan istirahat yang cukup.",
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 15,
                      letterSpacing: 1.5,
                      height: 1.5,
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: Container(
              child: Column(
                children: [
                  Container(
                    child: Image(
                        image: AssetImage('assets/videoinfog/JagaJarak.png')),
                    height: 250,
                    width: 250,
                    margin: EdgeInsets.only(bottom: 20),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 15),
                    child: Text(
                      "Physical Distancing",
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Text(
                    "Physical distancing adalah salah satu langkah penting untuk memutus mata rantai penyebaran virus Corona. Physical distancing juga dilakukan dengan cara menjaga jarak minimal 1 meter dengan orang lain dan selalu menggunakan masker, terutama saat beraktivitas di tempat umum atau keramaian.",
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 15,
                      letterSpacing: 1.5,
                      height: 1.5,
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 80.0),
            child: Container(
              child: Column(
                children: [
                  Container(
                    child: Image(
                        image: AssetImage('assets/videoinfog/BersihRumah.png')),
                    height: 250,
                    width: 250,
                    margin: EdgeInsets.only(bottom: 20),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 15),
                    child: Text(
                      "Membersihkan Rumah Secara Rutin",
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Text(
                    "Selain kebersihan diri, menjaga kebersihan rumah juga sangat penting dilakukan selama pandemi COVID-19 berlangsung. Hal ini dikarenakan virus Corona terbukti dapat bertahan hidup selama berjam-jam dan bahkan berhari-hari di permukaan suatu benda. Oleh karena itu, rumah harus dibersihkan dan dilakukan disinfeksi secara menyeluruh secara rutin.",
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 15,
                      letterSpacing: 1.5,
                      height: 1.5,
                    ),
                  )
                ],
              ),
            ),
          ),
          Container(
            color: Colors.white,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(80),
                  child: Text(
                    "WATCH OUR VIDEOS",
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        fontSize: 30,
                        letterSpacing: 1.5,
                        fontWeight: FontWeight.bold,
                        color: Color.fromARGB(255, 36, 77, 45)),
                  ),
                ),
                Container(
                  height: 1000,
                  child: FutureBuilder<List<Video>>(
                    future: vid.getVideo(),
                    builder: (context, snapshot) {
                      var data = snapshot.data;
                      return Expanded(
                          child: ListView.builder(
                        itemCount: data?.length,
                        itemBuilder: (context, index) {
                          if (!snapshot.hasData) {
                            return const Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          YoutubePlayerController _controller =
                              YoutubePlayerController(
                            initialVideoId: YoutubePlayer.convertUrlToId(
                                    "${data?[index].fields!.url}")
                                .toString(),
                            flags: YoutubePlayerFlags(
                              autoPlay: false,
                              mute: false,
                            ),
                          );
                          return Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: YoutubePlayer(
                              controller: _controller,
                              liveUIColor: Colors.amber,
                              showVideoProgressIndicator: true,
                            ),
                          );
                        },
                      ));
                    },
                  ),
                ),
                FloatingActionButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const VideoForm()));
                  },
                  backgroundColor: const Color(0xFFFDAF67),
                  child: const Icon(Icons.add),
                ),
              ],
            ),
            // decoration: BoxDecoration(
            //   image: DecorationImage(
            //     image: AssetImage("pattern-randomized.svg"),
            //     fit: BoxFit.cover,
            //   ),
            // ),
          )
        ],
      ),
    );
  }
}
