import 'dart:convert';

import 'package:http/http.dart' as http;
import 'video.dart';

class FetchVideo {
  var data = [];
  List<Video> result = [];

  String fecthUrl = 'https://zonahijau.herokuapp.com/videoinfog/json/';

  Future<List<Video>> getVideo() async {
    var url = Uri.parse(fecthUrl);
    var response = await http.get(url);

    try {
      if (response.statusCode == 200) {
        data = json.decode(response.body);
        result = data.map((e) => Video.fromJson(e)).toList();
      } else {
        print("api error");
      }
    } on Exception catch (e) {
      print('error: $e');
    }

    return result;
  }
}
