import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class VideoForm extends StatefulWidget {
  const VideoForm({Key? key}) : super(key: key);

  @override
  _VideoFormState createState() => _VideoFormState();
}

class _VideoFormState extends State<VideoForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;
  String? judul = "";
  String? link = "";
  String value = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Video Form"),
        backgroundColor: Colors.teal[600],
      ),
      body: Center(
          child: Container(
        width: MediaQuery.of(context).size.width / 2,
        child: Card(
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "INSERT VIDEO",
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        decoration: new InputDecoration(
                          hintText: "contoh: Push Up Vol.1",
                          labelText: "Video Tittle ...",
                          icon: Icon(
                            Icons.smart_display_outlined,
                          ),
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Video Tittle is Empty';
                          }
                          return null;
                        },
                        onChanged: (value) {
                          setState(() {
                            judul = value;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        decoration: new InputDecoration(
                          hintText:
                              "contoh: https://www.youtube.com/watch?v=lfspwEpJlZM",
                          labelText: "Video Link ...",
                          icon: Icon(
                            Icons.add_link_outlined,
                          ),
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Video Link is Empty';
                          }
                          return null;
                        },
                        onChanged: (value) {
                          setState(() {
                            link = value;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: SizedBox(
                        width: 170,
                        height: 40,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Color.fromRGBO(253, 175, 103, 1),
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30),
                              ),
                            ),
                            onPressed: () async {
                              String postUrl =
                                  "https://zonahijau.herokuapp.com/videoinfog/add_from_flutter";

                              var url = Uri.parse(postUrl);

                              if (_formKey.currentState!.validate()) {
                                final response = await http.post(url,
                                    body: json.encode({
                                      'judul': judul,
                                      'link': link,
                                    }));
                                _formKey.currentState?.reset();
                                Navigator.pop(context);
                              }
                            },
                            child: Text('Tambahkan')),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      )),
    );
  }
}
