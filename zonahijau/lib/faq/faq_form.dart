import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;

class FAQForm extends StatefulWidget {
  const FAQForm({Key? key}) : super(key: key);

  @override
  _FAQFormState createState() => _FAQFormState();
}

class _FAQFormState extends State<FAQForm> {
  final formKey = GlobalKey<FormState>();
  String? pertanyaan = "";
  String? jawaban = "";
  int? likes = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add COVID-19 FAQ"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // untuk judul
            Container(
              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Text(
                        "ADD",
                        style: GoogleFonts.sora(
                          textStyle: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Text(
                        "COVID-19 FAQ",
                        style: GoogleFonts.sora(
                          textStyle: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            // untuk form Add FAQ
            Container(
              margin: EdgeInsets.all(20),
              width: double.infinity,
              child: Card(
                child: Form(
                    key: formKey,
                    child: SingleChildScrollView(
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Column(
                          children: [
                            // untuk pertanyaan
                            Padding(
                              padding: EdgeInsets.all(8),
                              child: TextFormField(
                                maxLines: 2,
                                decoration: InputDecoration(
                                  hintText: "Write your question here...",
                                  labelText: "Question...",
                                  // icon: Icon(Icons.help),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5)),
                                ),
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return "Pertanyaan tidak boleh kosong";
                                  }
                                  return null;
                                },
                                onChanged: (value) {
                                  setState(() {
                                    pertanyaan = value;
                                  });
                                },
                              ),
                            ),
                            // untuk jawaban
                            Padding(
                              padding: EdgeInsets.all(8),
                              child: TextFormField(
                                maxLines: 10,
                                decoration: InputDecoration(
                                  hintText: "Write your answer here...",
                                  labelText: "Answer...",
                                  // icon: Icon(Icons.question_answer),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5)),
                                ),
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return "Jawaban tidak boleh kosong";
                                  }
                                  return null;
                                },
                                onChanged: (value) {
                                  setState(() {
                                    jawaban = value;
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    )),
              ),
            ),
            // Row untuk button "Add" dan "Cancel"
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // button untuk add
                TextButton(
                  child: Text(
                    "Add",
                    style: GoogleFonts.sora(
                      textStyle: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                  onPressed: () async {
                    // untuk save ke Django
                    String postUrl =
                        "https://zonahijau.herokuapp.com/faq/add_from_flutter";
                    var url = Uri.parse(postUrl);
                    if (formKey.currentState!.validate()) {
                      final response = await http.post(url,
                          body: json.encode({
                            'question': pertanyaan,
                            'answer': jawaban,
                            'likeNum': likes,
                          }));
                      formKey.currentState?.reset();
                      Navigator.pop(context);
                    }
                  },
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(Colors.grey.shade900)),
                ),
                // spacing antar button
                SizedBox(
                  width: 20,
                ),
                // button untuk cancel
                TextButton(
                  child: Text(
                    "Cancel",
                    style: GoogleFonts.sora(
                      textStyle: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.red)),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
