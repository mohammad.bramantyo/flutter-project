import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FAQWidget extends StatelessWidget {
  final String pertanyaan;
  final String jawaban;
  final int likes;
  final VoidCallback addLike;
  final textStyle;

  const FAQWidget({
    Key? key,
    required this.pertanyaan,
    required this.jawaban,
    required this.addLike,
    required this.likes,
    required this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      child: Column(
        children: [
          // untuk pertanyaan
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xff41b3a3),
            ),
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    child: Flexible(
                  child: Text(
                    pertanyaan,
                    textAlign: TextAlign.justify,
                    style: GoogleFonts.sora(
                      textStyle: textStyle,
                    ),
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
                )),
              ],
            ),
            height: 50,
            width: double.infinity,
          ),
          // untuk jawaban
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xfff4f4f4),
            ),
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    child: Flexible(
                  child: Text(
                    jawaban,
                    textAlign: TextAlign.justify,
                    style: GoogleFonts.sora(
                      textStyle: textStyle,
                    ),
                    maxLines: 9,
                    overflow: TextOverflow.ellipsis,
                  ),
                )),
              ],
            ),
            height: 160,
            width: double.infinity,
          ),
          Container(
            // untuk button
            padding: EdgeInsets.only(bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextButton(
                  onPressed: this.addLike,
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(Colors.grey.shade900)),
                  child: Text(
                    this.likes.toString() + " Likes",
                    style: GoogleFonts.sora(
                      textStyle: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                )
              ],
            ),
            height: 50,
            width: double.infinity,
            color: Color(0xfff4f4f4),
          ),
        ],
      ),
    );
  }
}
