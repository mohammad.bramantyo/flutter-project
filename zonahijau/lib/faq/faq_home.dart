import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:zonahijau/faq/faq_form.dart';
import 'package:zonahijau/faq/faq_model.dart';
import 'package:zonahijau/faq/faq_widget.dart';
import 'package:google_fonts/google_fonts.dart';

class FAQApp extends StatefulWidget {
  const FAQApp({Key? key}) : super(key: key);

  @override
  _FAQAppState createState() => _FAQAppState();
}

class _FAQAppState extends State<FAQApp> {
  List<FAQ> lstFAQ = [];

  @override
  void initState() {
    loopFunc();
  }

  Future<List<dynamic>> fetchFAQ() async {
    final response =
        await http.get(Uri.parse('https://zonahijau.herokuapp.com/faq/json'));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      List<dynamic> faqMap = jsonDecode(response.body);
      return faqMap;
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load FAQ');
    }
  }

  void loopFunc() {
    fetchFAQ().then((List<dynamic> value) {
      for (var i = 0; i < value.length; i++) {
        var faq = FAQ.fromJson(value[i]);
        lstFAQ.add(faq);
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("COVID-19 FAQ"),
      ),
      body: Container(
        child: Column(
          children: [
            // untuk judul
            Container(
              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Text(
                        "COVID-19",
                        style: GoogleFonts.sora(
                          textStyle: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Text(
                        "Frequently Asked Questions",
                        style: GoogleFonts.sora(
                          textStyle: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            // untuk button "Add FAQ"
            Container(
              height: 50,
              width: double.infinity,
              // color: Color(0xfff4f4f4),
              margin: EdgeInsets.fromLTRB(10, 0, 20, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.teal.shade500)),
                      child: Text(
                        "Add FAQ?",
                        style: GoogleFonts.sora(
                          textStyle: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                      ),
                      onPressed: () {
                        Navigator.of(context)
                            .push(MaterialPageRoute(
                                builder: (context) => const FAQForm()))
                            .then((value) {
                          lstFAQ = [];
                          loopFunc();
                        });
                      })
                ],
              ),
            ),
            // untuk objek FAQ
            (lstFAQ.isNotEmpty)
                ? Expanded(
                    child: Container(
                      child: ListView.builder(
                          itemCount: lstFAQ.length,
                          itemBuilder: (context, i) {
                            return FAQWidget(
                                pertanyaan: lstFAQ[i].fields!.question!,
                                jawaban: lstFAQ[i].fields!.answer!,
                                addLike: () async {
                                  var id = lstFAQ[i].pk!;
                                  final response = await http.get(Uri.parse(
                                      'https://zonahijau.herokuapp.com/faq/update_like/$id'));

                                  if (response.statusCode == 200) {
                                    // If the server did return a 200 OK response,
                                    // then parse the JSON.
                                    List<dynamic> faqMap =
                                        jsonDecode(response.body);

                                    lstFAQ[i].fields!.likeNum =
                                        faqMap[i]["fields"]["likeNum"];

                                    lstFAQ = [];
                                    loopFunc();
                                  } else {
                                    // If the server did not return a 200 OK response,
                                    // then throw an exception.
                                    throw Exception('Failed to load FAQ');
                                  }
                                },
                                likes: lstFAQ[i].fields!.likeNum!,
                                textStyle: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold));
                          }),
                    ),
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          ],
        ),
      ),
    );
  }
}
