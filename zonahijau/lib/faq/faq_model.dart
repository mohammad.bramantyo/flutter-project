class FAQ {
  String? model;
  int? pk;
  Fields? fields;

  FAQ({this.model, this.pk, this.fields});

  FAQ.fromJson(Map<String, dynamic> json) {
    model = json['model'];
    pk = json['pk'];
    fields =
        json['fields'] != null ? new Fields.fromJson(json['fields']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['model'] = this.model;
    data['pk'] = this.pk;
    if (this.fields != null) {
      data['fields'] = this.fields!.toJson();
    }
    return data;
  }
}

class Fields {
  String? question;
  String? answer;
  int? likeNum;

  Fields({this.question, this.answer, this.likeNum});

  Fields.fromJson(Map<String, dynamic> json) {
    question = json['question'];
    answer = json['answer'];
    likeNum = json['likeNum'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['question'] = this.question;
    data['answer'] = this.answer;
    data['likeNum'] = this.likeNum;
    return data;
  }
}
