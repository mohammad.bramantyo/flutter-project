import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:zonahijau/mitosfakta/mitosfakta_model.dart';

class FetchMitosFaktaSoal {
  var data = [];
  List<JsonSoal> result = [];

  String fecthUrl = 'https://zonahijau.herokuapp.com/mitosfakta/json_soal';

  Future<List<JsonSoal>> getSoal() async {
    var url = Uri.parse(fecthUrl);
    var response = await http.get(url);

    try {
      if (response.statusCode == 200) {
        data = json.decode(response.body);
        result = data.map((e) => JsonSoal.fromJson(e)).toList();
      } else {
        print("api error");
      }
    } on Exception catch (e) {
      print('error: $e');
    }

    return result;
  }
}

/////
class FetchMitosFaktaLeaderboard {
  var data = [];
  List<JsonLeaderboard> result = [];

  String fecthUrl =
      'https://zonahijau.herokuapp.com/mitosfakta/json_leaderboard';

  Future<List<JsonLeaderboard>> getLeaderboard() async {
    var url = Uri.parse(fecthUrl);
    var response = await http.get(url);

    try {
      if (response.statusCode == 200) {
        data = json.decode(response.body);
        result = data.map((e) => JsonLeaderboard.fromJson(e)).toList();
      } else {
        print("api error");
      }
    } on Exception catch (e) {
      print('error: $e');
    }

    return result;
  }
}
