import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:zonahijau/mitosfakta/mitosfakta_add.dart';
import 'package:zonahijau/mitosfakta/mitosfakta_leaderboard.dart';
// import 'package:zonahijau/user/cookieReq.dart';
import 'mitosfakta_quiz.dart';

class MitosFaktaHome extends StatefulWidget {
  const MitosFaktaHome({Key? key}) : super(key: key);

  @override
  _MitosFaktaHomeState createState() => _MitosFaktaHomeState();
}

class _MitosFaktaHomeState extends State<MitosFaktaHome> {
  @override
  Widget build(BuildContext context) {
    // CookieRequest cookie = context.read<CookieRequest>();
    return Scaffold(
      appBar: AppBar(
        title: Text("ZonaHijau"),
        backgroundColor: Colors.teal[300],
      ),
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width / 1.1,
          child: Card(
            margin: const EdgeInsets.all(2.0),
            elevation: 3.0,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 1.0),
                  child: const Text(
                    "Mitos atau Fakta?",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                ),
                const Divider(
                  color: Colors.black,
                  indent: 10,
                  endIndent: 10,
                ),
                const Padding(
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: Text(
                    "Quiz ini akan membahas mengenai mitos dan fakta seputar Covid-19 yang beredar pada masyarakat.",
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(top: 15),
                    width: MediaQuery.of(context).size.width / 1.4,
                    child: Image.asset(
                        'assets/mitosfakta/mitosfaktacovidimg400.jpg')),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 3),
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.blue)),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MitosFaktaQuiz()));
                    },
                    child: const Text('Start Quiz'),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 3),
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.blue)),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  const MitosFaktaLeaderboard()));
                    },
                    child: const Text('Lihat Leaderboard'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 15),
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.blue)),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MitosFaktaAdd()));
                    },
                    child: const Text('Tambah Pertanyaan'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
