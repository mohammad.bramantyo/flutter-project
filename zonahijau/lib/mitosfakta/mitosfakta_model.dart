class JsonSoal {
  String? model;
  int? pk;
  Fields? fields;

  JsonSoal({this.model, this.pk, this.fields});

  JsonSoal.fromJson(Map<String, dynamic> json) {
    model = json['model'];
    pk = json['pk'];
    fields =
        json['fields'] != null ? new Fields.fromJson(json['fields']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['model'] = this.model;
    data['pk'] = this.pk;
    if (this.fields != null) {
      data['fields'] = this.fields!.toJson();
    }
    return data;
  }
}

class Fields {
  String? question;
  String? answer;
  String? pembahasan;
  String? nomor;

  Fields({this.question, this.answer, this.pembahasan, this.nomor});

  Fields.fromJson(Map<String, dynamic> json) {
    question = json['question'];
    answer = json['answer'];
    pembahasan = json['pembahasan'];
    nomor = json['nomor'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['question'] = this.question;
    data['answer'] = this.answer;
    data['pembahasan'] = this.pembahasan;
    data['nomor'] = this.nomor;
    return data;
  }
}

///////
class JsonLeaderboard {
  int? rank;
  String? name;
  int? score;

  JsonLeaderboard({this.rank, this.name, this.score});

  JsonLeaderboard.fromJson(Map<String, dynamic> json) {
    rank = json['rank'];
    name = json['name'];
    score = json['score'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rank'] = this.rank;
    data['name'] = this.name;
    data['score'] = this.score;
    return data;
  }
}
