import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class MitosFaktaAdd extends StatefulWidget {
  const MitosFaktaAdd({Key? key}) : super(key: key);

  @override
  _MitosFaktaAddState createState() => _MitosFaktaAddState();
}

class _MitosFaktaAddState extends State<MitosFaktaAdd> {
  final _formKey = GlobalKey<FormState>();
  String? question = "";
  String? answer = "";
  String? pembahasan = "";
  String? nomor = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("ZonaHijau"),
        backgroundColor: Colors.teal[300],
      ),
      body: Center(
          child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 20, 10, 15),
            child: Text(
              "Tambah Pertanyaan Quiz",
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.1,
            child: Card(
              child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                              hintText: "Masukkan pertanyaan",
                              labelText: "Pertanyaan",
                              icon: Icon(Icons.help_outline),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0)),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Pertanyaan tidak boleh kosong';
                              }
                              return null;
                            },
                            onChanged: (value) {
                              setState(() {
                                question = value;
                              });
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                              hintText: "mitos/fakta",
                              labelText: "Jawaban",
                              icon: Icon(Icons.question_answer_outlined),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0)),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Jawaban tidak boleh kosong';
                              }
                              return null;
                            },
                            onChanged: (value) {
                              setState(() {
                                answer = value;
                              });
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                              hintText: "Masukkan pembahasan",
                              labelText: "Alasan",
                              icon: Icon(Icons.grading),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0)),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Alasan tidak boleh kosong';
                              }
                              return null;
                            },
                            onChanged: (value) {
                              setState(() {
                                pembahasan = value;
                              });
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                              hintText: "Masukkan nomor soal",
                              labelText: "Nomor",
                              icon: Icon(Icons.format_list_numbered_sharp),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0)),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Nomor tidak boleh kosong';
                              }
                              return null;
                            },
                            onChanged: (value) {
                              setState(() {
                                nomor = value;
                              });
                            },
                          ),
                        ),
                        ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.teal[300])),
                          onPressed: () async {
                            String postUrl =
                                "https://zonahijau.herokuapp.com/mitosfakta/add_flutter";

                            var url = Uri.parse(postUrl);

                            if (_formKey.currentState!.validate()) {
                              final response = await http.post(url,
                                  body: json.encode({
                                    'question': question,
                                    'answer': answer,
                                    'pembahasan': pembahasan,
                                    'nomor': nomor
                                  }));
                              _formKey.currentState?.reset();
                              Navigator.pop(context);
                            }
                          },
                          child: const Text(
                            'Tambah',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      )),
    );
  }
}
