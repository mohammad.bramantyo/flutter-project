import 'package:flutter/material.dart';
import 'package:zonahijau/mitosfakta/MethodSoal.dart';
import 'package:zonahijau/mitosfakta/mitosfakta_leaderboard.dart';
import 'package:zonahijau/mitosfakta/mitosfakta_model.dart';

class MitosFaktaResult extends StatefulWidget {
  String finalScore;
  MitosFaktaResult(this.finalScore);

  // const MitosFaktaResult({Key? key, @required this.finalScore})
  //     : super(key: key);

  @override
  _MitosFaktaResultState createState() =>
      _MitosFaktaResultState(this.finalScore);
}

class _MitosFaktaResultState extends State<MitosFaktaResult> {
  String finalScore;
  _MitosFaktaResultState(this.finalScore);
  final FetchMitosFaktaSoal result = FetchMitosFaktaSoal();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("ZonaHijau"),
          backgroundColor: Colors.teal[300],
        ),
        body: Container(
            child: Card(
                margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                elevation: 3.0,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 5.0),
                      child: Text(
                        "Your Score = " + finalScore,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ),
                    const Divider(
                      color: Colors.black,
                      indent: 10,
                      endIndent: 10,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(10, 0, 10, 5),
                      child: Text(
                        "Pembahasan:",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    Container(
                        child: FutureBuilder<List<JsonSoal>>(
                            future: result.getSoal(),
                            builder: (context, snapshot) {
                              var data = snapshot.data;
                              return Expanded(
                                child: ListView.builder(
                                    itemCount: data?.length,
                                    itemBuilder: (context, index) {
                                      if (!snapshot.hasData) {
                                        return const Center(
                                            child: CircularProgressIndicator());
                                      }
                                      return Card(
                                          margin: const EdgeInsets.fromLTRB(
                                              30.0, 10.0, 30.0, 10.0),
                                          elevation: 5.0,
                                          shape: RoundedRectangleBorder(
                                            side: BorderSide(
                                                color: Colors.green, width: 1),
                                          ),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Container(
                                                padding: EdgeInsets.fromLTRB(
                                                    10.0, 10.0, 10.0, 1.0),
                                                child: Text(
                                                  "Pernyataan nomor " +
                                                      '${data?[index].fields!.nomor}:',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 15),
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                    10, 0, 10, 10),
                                                child: Text(
                                                  '${data?[index].fields!.question}',
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                    10, 0, 10, 1),
                                                child: Text(
                                                  'Penjelasan:',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 15),
                                                ),
                                              ),
                                              Container(
                                                color: Colors.green,
                                                padding: EdgeInsets.fromLTRB(
                                                    10, 10, 10, 10),
                                                child: Text(
                                                  '${data?[index].fields!.pembahasan}',
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                            ],
                                          ));
                                    }),
                              );
                            })),
                    Container(
                      margin: EdgeInsets.all(15),
                      child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.blue)),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      MitosFaktaLeaderboard()));
                        },
                        child: const Text('Lihat Leaderboard'),
                      ),
                    ),
                  ],
                ))));
  }
}
