import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:zonahijau/mitosfakta/MethodSoal.dart';
import 'package:zonahijau/mitosfakta/mitosfakta_model.dart';
import 'package:zonahijau/mitosfakta/mitosfakta_result.dart';
// import 'package:zonahijau/user/cookieReq.dart';
import 'dart:convert';

class MitosFaktaQuiz extends StatefulWidget {
  const MitosFaktaQuiz({Key? key}) : super(key: key);

  @override
  _MitosFaktaQuizState createState() => _MitosFaktaQuizState();
}

class _MitosFaktaQuizState extends State<MitosFaktaQuiz> {
  final FetchMitosFaktaSoal soal = FetchMitosFaktaSoal();
  bool visible = false;
  bool isEnabled = true;
  num score = 0;
  var count = 0;

  void checkAnswer(String choice, String ans, int len, BuildContext context) {
    if (choice == ans) {
      score++;
    }
    setState(() {
      if (count < len - 1) {
        count++;
        const snackbar = SnackBar(
          content: Text(
            "Jawaban Tersimpan",
            textAlign: TextAlign.center,
          ),
          duration: Duration(milliseconds: 300),
          backgroundColor: Colors.blue,
        );
        // ignore: deprecated_member_use
        Scaffold.of(context).showSnackBar(snackbar);
      } else {
        const snackbar = SnackBar(
          content: Text(
            "Jawaban Tersimpan",
            textAlign: TextAlign.center,
          ),
          duration: Duration(milliseconds: 300),
          backgroundColor: Colors.blue,
        );
        // ignore: deprecated_member_use
        Scaffold.of(context).showSnackBar(snackbar);
        isEnabled = false;
        visible = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // CookieRequest cookie = context.read<CookieRequest>();

    return Scaffold(
      appBar: AppBar(
        title: Text("ZonaHijau"),
        backgroundColor: Colors.teal[300],
      ),
      body: Container(
          child: FutureBuilder<List<JsonSoal>>(
        future: soal.getSoal(),
        builder: (context, snapshot) {
          var data = snapshot.data;
          if (!snapshot.hasData) {
            return const Center(child: CircularProgressIndicator());
          }
          return Center(
            child: Card(
              margin: const EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 10.0),
              elevation: 3.0,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          "${data?[count].fields!.nomor}/${data?.length}",
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          " Question",
                          style: TextStyle(fontSize: 10),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 200,
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 1.0),
                    child: Text(
                      "Mitos atau Fakta? ",
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                  const Divider(
                    color: Colors.black,
                    indent: 10,
                    endIndent: 10,
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 3, 10, 0),
                    child: Text(
                      '${data?[count].fields!.question}',
                      textAlign: TextAlign.center,
                    ),
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.red)),
                        onPressed: isEnabled
                            ? () => checkAnswer(
                                "mitos",
                                data![count].fields!.answer!,
                                data.length,
                                context)
                            : null,
                        child: const Text(
                          'Mitos',
                        ),
                      ),
                      ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.green)),
                        onPressed: isEnabled
                            ? () => checkAnswer(
                                "fakta",
                                data![count].fields!.answer!,
                                data.length,
                                context)
                            : null,
                        child: const Text('Fakta'),
                      ),
                    ],
                  ),
                  const SizedBox(height: 15),
                  if (visible)
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.blue)),
                        onPressed: () async {
                          score = (score * (100 / data!.length));
                          var scoreInt = score.toInt();
                          String scoreStr = scoreInt.toString();
                          // final response = await cookie.post(
                          //     "https://zonahijau.herokuapp.com/mitosfakta/update_score",
                          //     json.encode({
                          //       'username': cookie.username,
                          //       'score': scoreStr
                          //     }));
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      MitosFaktaResult(scoreInt.toString())));
                        },
                        child: const Text('Submit Quiz'),
                      ),
                    ),
                ],
              ),
            ),
          );
        },
      )),
    );
  }
}
