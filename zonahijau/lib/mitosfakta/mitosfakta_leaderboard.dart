import 'package:flutter/material.dart';
import 'package:zonahijau/mitosfakta/MethodSoal.dart';
import 'package:zonahijau/mitosfakta/mitosfakta_model.dart';

class MitosFaktaLeaderboard extends StatefulWidget {
  const MitosFaktaLeaderboard({Key? key}) : super(key: key);

  @override
  _MitosFaktaLeaderboardState createState() => _MitosFaktaLeaderboardState();
}

class _MitosFaktaLeaderboardState extends State<MitosFaktaLeaderboard> {
  final FetchMitosFaktaLeaderboard leaderboard = FetchMitosFaktaLeaderboard();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("ZonaHijau"),
          backgroundColor: Colors.teal[300],
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text(
                  "Leaderboard",
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                ),
              ),
              Card(
                elevation: 5,
                child: Card(
                  margin: const EdgeInsets.fromLTRB(10, 10, 10, 5),
                  child: FutureBuilder<List<JsonLeaderboard>>(
                      future: leaderboard.getLeaderboard(),
                      builder: (context, snapshot) {
                        var data = snapshot.data;
                        if (!snapshot.hasData) {
                          return const Center(
                              child: CircularProgressIndicator());
                        }
                        return Expanded(
                          child: SizedBox(
                              height: MediaQuery.of(context).size.height / 1.3,
                              child: SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: Theme(
                                  data: Theme.of(context)
                                      .copyWith(dividerColor: Colors.black),
                                  child: DataTable(
                                    showBottomBorder: true,
                                    headingRowColor:
                                        MaterialStateProperty.all(Colors.green),
                                    columns: const [
                                      DataColumn(
                                          label: Expanded(
                                        child: Text(
                                          "Rank",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      )),
                                      DataColumn(
                                          label: Expanded(
                                        child: Text("Nama",
                                            textAlign: TextAlign.center,
                                            style:
                                                TextStyle(color: Colors.white)),
                                      )),
                                      DataColumn(
                                          label: Expanded(
                                        child: Text("Skor",
                                            textAlign: TextAlign.center,
                                            style:
                                                TextStyle(color: Colors.white)),
                                      ))
                                    ],
                                    rows: data!
                                        .map((data) => DataRow(
                                                color: MaterialStateColor
                                                    .resolveWith((states) =>
                                                        data.rank! % 2 != 0
                                                            ? Colors.white
                                                            : Colors
                                                                .grey[100]!),
                                                cells: [
                                                  DataCell(Center(
                                                    child: Text(
                                                      '${data.rank}',
                                                    ),
                                                  )),
                                                  DataCell(Center(
                                                    child: Text(
                                                      '${data.name}',
                                                    ),
                                                  )),
                                                  DataCell(Center(
                                                    child: Text(
                                                      '${data.score}',
                                                    ),
                                                  ))
                                                ]))
                                        .toList(),
                                  ),
                                ),
                              )),
                        );
                      }),
                ),
              ),
            ],
          ),
        ));
  }
}
