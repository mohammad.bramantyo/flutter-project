import 'package:flutter/material.dart';
import 'faq/faq_home.dart';
import 'rumahsakit/rumahsakit.dart';
import 'fufu/fufu_index.dart';
import 'dashboard/dashboard1.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ZonaHijau',
      theme: ThemeData(
        primaryColor: const Color(0xFF7CC1AC),
        primarySwatch: Colors.teal,
      ),
      home: DashboardApp(), // Ganti aja ini kalo mau ngecek app kalian
      // Scaffold(
      //     appBar: AppBar(
      //       title: const Text("ZonaHijau"),
      //     ),
      //     body: Container(
      //       constraints: const BoxConstraints.expand(),
      //       decoration: const BoxDecoration(
      //           image: DecorationImage(
      //         image: AssetImage("images/Logo.png"),
      //       )),
      //     ))
    );
  }
}
