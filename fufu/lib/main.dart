import 'package:flutter/material.dart';
import 'package:fufu/fufu_index.dart';

void main() {
  runApp(MaterialApp(
    title: "ZonaHijau",
    home: FufuDriver(),
    theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: const Color(0xFF7CC1AC),
        fontFamily: 'Sora',
        textTheme: const TextTheme(
          headline1: TextStyle(
            fontFamily: "Sora",
            fontSize: 25,
            fontWeight: FontWeight.bold,
            color: Colors.black),
          bodyText1: TextStyle(
            fontFamily: "Sora",
            fontSize: 18,
            fontWeight: FontWeight.normal,
            color: Colors.black),
          headline2: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.bold,
            color: Colors.black),
          bodyText2: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.normal,
            color: Colors.black54),
          button: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.normal,
            color: Colors.blue),
        )
      ),
  ));
}

class FufuDriver extends StatefulWidget {

  final state = _FufuDriverState();

  @override
  State<StatefulWidget> createState() => state;
}

class _FufuDriverState extends State<FufuDriver> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              child: const Text('Fufu'),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => Fufu()));
              },
            ),
          ],
        ),
      ),
    );
  }
}