import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:carousel_slider/carousel_slider.dart';
import 'package:fufu/fufu_see_all.dart';
import 'package:fufu/fufu_model.dart';

class Fufu extends StatefulWidget {
  
  @override
  State<StatefulWidget> createState() =>  _FufuState();
}

Future<List<dynamic>> getFufus() async {
    final response = await http.get(Uri.parse('https://zonahijau.herokuapp.com/fufuJson'));
    List<dynamic> result = [];
    try {
      if (response.statusCode == 200) {
        result = jsonDecode(utf8.decode(response.bodyBytes)).map((e) => FufuModel.fromJson(e)).toList();
      } else {
        print("api error");
      }
    } on Exception catch (e) {
      print('error: $e');
    }

    return result;
}

class _FufuState extends State<Fufu> {

  late Future<List<dynamic>> futureFufus;
  
  @override
  void initState() {
    super.initState();
    futureFufus = getFufus();
  }

  @override
  Widget build(BuildContext context) {
    final Widget fufuSlider = FutureBuilder<List<dynamic>>(
      future: futureFufus,
      builder: (context, snapshot) {
        if(snapshot.hasData) {
          return CarouselSlider.builder(
              options: CarouselOptions(
                  height: 400,
                  autoPlay: true,
                  autoPlayInterval: const Duration(seconds: 8),
                  enlargeCenterPage: true,
                ),
              itemCount: snapshot.data?.length,
              itemBuilder: (BuildContext context, int itemIndex, int pageViewIndex) =>
                Container(
                  width: 1000,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 2.0),
                    borderRadius: const BorderRadius.all(Radius.circular(5.0))
                  ),
                  margin: const EdgeInsets.all(5.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(30.0).copyWith(bottom: 20),
                        child: RichText(
                          text: TextSpan(
                            text: "From: ${snapshot.data?[itemIndex].fields?.from}",
                            style: Theme.of(context).textTheme.headline2),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 0, bottom: 20.0),
                        child: Text(
                          "${snapshot.data?[itemIndex].fields?.message}",
                          style: Theme.of(context).textTheme.bodyText2
                        ),
                      ),
                    ],
                  ),
                )
              );
        }
        return const Center(
          child: CircularProgressIndicator(color: Color(0xFF7CC1AC)),
          );
        }
      );

    return Scaffold(
      appBar: AppBar(
        title: const Text("Fufu"),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Center(
        child: Column (
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 15.0, bottom: 10.0),
              child: Text(
                "From Us For Us (FUFU)",
                textAlign: TextAlign.left,
                style: Theme.of(context).textTheme.headline1,
                ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 25.0, bottom: 10.0),
              child: Text(
                "A simple message to cheerish each other \nduring pandemic",
                style: Theme.of(context).textTheme.bodyText1,
                ),
            ),
            Align(
              alignment:  Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.only(right: 30),
                child: TextButton(
                  style: TextButton.styleFrom(
                      textStyle: const TextStyle(fontSize: 18),
                      splashFactory: NoSplash.splashFactory,
                    ),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => SeeAllFufu()));
                  },
                  child: const Text('See All'),
                ),
              )
            ),
            fufuSlider
            ],
          ),
        ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // route to add fufu
        },
        backgroundColor: Theme.of(context).primaryColor,
        child: const Icon(Icons.add),
      )
    );
  }
}