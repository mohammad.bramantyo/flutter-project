import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:expandable/expandable.dart';
import 'package:fufu/fufu_model.dart';

class SeeAllFufu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SeeAllFufuState();
}

Future<List<dynamic>> getFufus() async {
    final response = await http.get(Uri.parse('https://zonahijau.herokuapp.com/fufuJson'));
    List<dynamic> result = [];
    try {
      if (response.statusCode == 200) {
        result = jsonDecode(utf8.decode(response.bodyBytes)).map((e) => FufuModel.fromJson(e)).toList();
      } else {
        print("api error");
      }
    } on Exception catch (e) {
      print('error: $e');
    }

    return result;
}

class _SeeAllFufuState extends State<SeeAllFufu> {
  
  late Future<List<dynamic>> futureFufus;
  
  @override
  void initState() {
    super.initState();
    futureFufus = getFufus();
  }
  @override
  Widget build(BuildContext context) {
    
    final Widget fufuSeeAll = FutureBuilder<List<dynamic>>(
      future: futureFufus,
      builder: (context, snapshot) {
        if(snapshot.hasData) {
          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 25.0, left: 15.0, bottom: 20.0),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "See All Fufu",
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.headline1,
                      ),
                  ),
                ),
                ...snapshot.data!.map((item) => ExpandableNotifier(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      side: const BorderSide(color: Colors.black, width: 2),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    elevation: 6,
                    shadowColor: Theme.of(context).primaryColor,
                    margin: const EdgeInsets.only(left:10, right: 10, top: 5, bottom: 5),
                    child: ScrollOnExpand(
                      child: ExpandablePanel(
                        theme: const ExpandableThemeData(
                          tapBodyToExpand: true,
                        ),
                      header: Align(
                          child: Padding(
                            padding: const EdgeInsets.all(30).copyWith(bottom: 20),
                            child: Text(
                                  "From: ${item.fields!.from}",
                                  style: Theme.of(context).textTheme.headline2,
                                ),
                          ),
                          alignment: Alignment.centerLeft,
                        ),
                      collapsed: Padding(
                        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 0, bottom: 20.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            item.fields!.message,
                            style: Theme.of(context).textTheme.bodyText2,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                      expanded: Padding(
                        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 0, bottom: 20.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            item.fields!.message,
                            style: Theme.of(context).textTheme.bodyText2,
                          ),
                        ),
                      ),
                        ),
                    )
                  ),
                )
                ),
              ],
              ),
          );
        }
        return const Center(
          child: CircularProgressIndicator(color: Color(0xFF7CC1AC)),
          );
        }
      );

    return Scaffold(
      appBar: AppBar(
        title: const Text("From Us For Us (FUFU)"),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: fufuSeeAll
    );
  }
}